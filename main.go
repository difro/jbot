package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	iconv "github.com/djimenez/iconv-go"
	"github.com/line/line-bot-sdk-go/linebot"
)

var (
	lineChannelSecret  = ""
	lineChannelToken   = ""
	naverOpenapiID     = ""
	naverOpenapiSecret = ""
)

func init() {
	lineChannelSecret = os.Getenv("LINE_CHANNEL_SECRET")
	lineChannelToken = os.Getenv("LINE_CHANNEL_TOKEN")
	naverOpenapiID = os.Getenv("NAVER_OPENAPI_ID")
	naverOpenapiSecret = os.Getenv("NAVER_OPENAPI_SECRET")
}

func printHelp(bot *linebot.Client, replyId string) {
	help := "제이봇! 알람 <seconds> <optional message>\n" +
		"제이봇! 영어로 <message>\n" +
		"제이봇! 한국어로 <message>\n"

	if _, err := bot.PushMessage(replyId, linebot.NewTextMessage(help)).Do(); err != nil {
		log.Print(err)
	}
}

func reply(bot *linebot.Client, token, message string) error {
	_, err := bot.ReplyMessage(token, linebot.NewTextMessage(message)).Do()
	return err
}

func getReplyId(event *linebot.Event) string {
	switch event.Source.Type {
	case linebot.EventSourceTypeUser:
		return event.Source.UserID
	case linebot.EventSourceTypeGroup:
		return event.Source.GroupID
	case linebot.EventSourceTypeRoom:
		return event.Source.RoomID
	}

	return ""
}

func getNaverNewsLink(query string) (link string, err error) {
	client := &http.Client{}

	reqUrl := fmt.Sprintf("%s%s",
		"https://openapi.naver.com/v1/search/news.json?start=1&display=1&sort=sim&query=",
		url.QueryEscape(query))
	log.Println(reqUrl)

	req, err := http.NewRequest("GET", reqUrl, nil)
	if err != nil {
		log.Println(err)
		return "", err
	}
	req.Header.Add("X-Naver-Client-Id", naverOpenapiID)
	req.Header.Add("X-Naver-Client-Secret", naverOpenapiSecret)

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return "", err
	}
	defer resp.Body.Close()

	type openapiNewsItem struct {
		Title        string `json:"title"`
		Originallink string `json:"originallink"`
		Link         string `json:"link"`
		Description  string `json:"description"`
		PubDate      string `json:"pub_date"`
	}
	type openapiNews struct {
		Total int               `json:"total"`
		Items []openapiNewsItem `json:"items"`
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return "", err
	}

	log.Println(string(body))

	j := &openapiNews{}
	if err := json.Unmarshal(body, j); err != nil {
		log.Println(err)
		return "", err
	}
	if j.Total <= 0 {
		return "", fmt.Errorf("No search results")
	}
	if strings.Contains(j.Items[0].Link, "news.naver.com") == false {
		return "", fmt.Errorf("Not naver news link: %s", j.Items[0].Link)
	}
	log.Println(j.Items[0].Link)

	return j.Items[0].Link, nil
}

func translate(text, source, target string) (string, error) {
	client := &http.Client{}

	reqUrl := "https://openapi.naver.com/v1/papago/n2mt"

	postString := "source=" + source + "&target=" + target + "&text=" + text
	log.Println(postString)
	req, err := http.NewRequest("POST", reqUrl, bytes.NewBuffer([]byte(postString)))
	if err != nil {
		log.Println(err)
		return "", err
	}
	req.Header.Add("X-Naver-Client-Id", naverOpenapiID)
	req.Header.Add("X-Naver-Client-Secret", naverOpenapiSecret)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return "", err
	}
	defer resp.Body.Close()

	type openapiTranslate struct {
		Message struct {
			Result struct {
				TranslatedText string `json:"translatedText"`
			} `json:"result"`
		} `json:"message"`
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return "", err
	}

	log.Println(string(body))
	j := &openapiTranslate{}
	if err := json.Unmarshal(body, j); err != nil {
		log.Println(err)
		return "", err
	}
	return j.Message.Result.TranslatedText, nil
}

func handleImages(bot *linebot.Client, event *linebot.Event, content *linebot.MessageContentResponse) error {

	// prepare multipart data
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	iw, err := w.CreateFormField("image")
	if err != nil {
		return err
	}
	if n, err := io.Copy(iw, content.Content); err != nil {
		log.Println("Copy error", err)
		return err
	} else {
		log.Printf("Copied %d bytes", n)
	}

	w.Close()

	client := &http.Client{}

	reqUrl := "https://openapi.naver.com/v1/vision/celebrity"
	//reqUrl := "http://localhost:33333/v1/vision/celebrity"
	log.Println(reqUrl)

	req, err := http.NewRequest("POST", reqUrl, &b)
	if err != nil {
		log.Println(err)
		return err
	}
	req.Header.Add("X-Naver-Client-Id", naverOpenapiID)
	req.Header.Add("X-Naver-Client-Secret", naverOpenapiSecret)
	req.Header.Set("Content-Type", w.FormDataContentType())

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return err
	}
	type celeb struct {
		Info struct {
			Size struct {
				Width  int
				Height int
			}
			FaceCount int
		}
		Faces []struct {
			Celebrity struct {
				Value      string
				Confidence float64
			}
		}
	}

	log.Println(string(body))

	j := &celeb{}
	if err = json.Unmarshal(body, j); err != nil {
		log.Println(err)
		return err
	}

	if len(j.Faces) <= 0 {
		return nil
	}

	var msgs []string
	for _, f := range j.Faces {
		if f.Celebrity.Confidence >= 0.5 {
			msgs = append(msgs, fmt.Sprintf("%s, %.2f", f.Celebrity.Value, f.Celebrity.Confidence))
		}
	}

	if len(msgs) == 0 {
		return nil
	}

	if _, err := bot.PushMessage(
		getReplyId(event),
		linebot.NewTextMessage(strings.Join(msgs, "\n")),
	).Do(); err != nil {
		log.Print(err)
		return err
	}
	return nil
}

func handleLinks(bot *linebot.Client, event *linebot.Event, message string) error {
	// handle arxiv
	arxivPdfLinks := regexp.MustCompile(`https?://arxiv\.org/pdf/(\d+\.\d+)\.pdf`)
	for _, m := range arxivPdfLinks.FindAllStringSubmatch(message, -1) {
		log.Println("Found arxiv pdf %v", m[1])
		if _, err := bot.PushMessage(getReplyId(event), linebot.NewTextMessage("https://arxiv.org/abs/"+m[1])).Do(); err != nil {
			log.Print(err)
		}
	}

	// handle news linkx
	if strings.Contains(message, "news.naver.com") == true {
		return fmt.Errorf("Already naver news link")
	}

	doc, err := goquery.NewDocument(message)
	if err != nil {
		log.Println(err)
	}

	title := ""
	// try retrieving og:title
	doc.Find("meta").Each(func(i int, s *goquery.Selection) {
		if name, _ := s.Attr("property"); name == "og:title" {
			title, _ = s.Attr("content")
			log.Printf("Found og:title [%s]", title)
		}
	})

	if title == "" {
		title = strings.TrimSpace(doc.Find("title").First().Text())
	}

	if title == "" {
		return fmt.Errorf("Can't get valid title")
	}

	_, err = iconv.ConvertString(title, "utf-8", "utf-8")
	if err != nil {
		// try cp949 convertion
		title, err = iconv.ConvertString(title, "cp949", "utf-8")
		if err != nil {
			return fmt.Errorf("Title not in supported encoding")
		}
	}

	// trying to remove company suffix
	if n := strings.LastIndex(title, " : "); n != -1 {
		title = title[:n]
	} else if n := strings.LastIndex(title, " - "); n != -1 {
		title = title[:n]
	}

	log.Println(title)

	naverLink, err := getNaverNewsLink(title)
	if err != nil {
		return fmt.Errorf("Can't find naver news link", err)
	}

	if _, err = bot.PushMessage(getReplyId(event), linebot.NewTextMessage(naverLink)).Do(); err != nil {
		log.Print(err)
	}

	return nil
}

func handleJbotCommands(bot *linebot.Client, event *linebot.Event, message string) error {
	tokens := strings.Split(message, " ")
	log.Printf("%d : %v\n", len(tokens), tokens)
	if len(tokens) < 2 && (tokens[0] == "/jbot" || tokens[0] == "제이봇!") {
		printHelp(bot, getReplyId(event))
		return fmt.Errorf("Not a jbot command")
	}

	cmd := tokens[1]

	switch cmd {
	case "help", "도와줘":
		printHelp(bot, getReplyId(event))
		return nil

	case "alarm", "알람":
		if len(tokens) < 3 {
			reply(bot, event.ReplyToken, "/jbot alarm <seconds> <optional message>")
			return nil
		}

		seconds, err := strconv.Atoi(tokens[2])
		if err != nil {
			reply(bot, event.ReplyToken, "/jbot alarm <seconds> <optional message>")
			return nil
		}

		message := "alarm!!"
		if len(tokens) > 3 {
			message = strings.Join(tokens[3:], " ")
		}

		reply(bot, event.ReplyToken, fmt.Sprintf("Will alert [%s] in %d sec.", message, seconds))

		replyId := getReplyId(event)

		go func(replyId, message string, seconds time.Duration) {
			time.Sleep(seconds)
			if _, err = bot.PushMessage(replyId, linebot.NewTextMessage(message)).Do(); err != nil {
				log.Print(err)
			}
		}(replyId, message, (time.Duration)(seconds)*time.Second)

	case "engkor", "koreng", "영어로", "한국어로":
		if len(tokens) < 3 {
			reply(bot, event.ReplyToken, "/jbot "+cmd+" <text>")
			return nil
		}

		go func(replyId, text, cmd string) error {
			var translated string
			var err error
			if cmd == "koreng" || cmd == "영어로" {
				translated, err = translate(text, "ko", "en")
			} else if cmd == "engkor" || cmd == "한국어로" {
				translated, err = translate(text, "en", "ko")
			} else {
				log.Println("Unknown cmd")
				return nil
			}

			if err != nil {
				return err
			}
			log.Println(translated)
			if _, err = bot.PushMessage(replyId, linebot.NewTextMessage(translated)).Do(); err != nil {
				log.Print(err)
				return err
			}
			return nil
		}(getReplyId(event), strings.Join(tokens[2:], " "), cmd)

	}

	return nil
}

func main() {
	bot, err := linebot.New(lineChannelSecret, lineChannelToken)
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		events, err := bot.ParseRequest(r)
		if err != nil {
			log.Printf("ParseRequest: %s", err)
			return
		}

		for _, event := range events {
			b, err := event.MarshalJSON()
			if err == nil {
				log.Println(string(b))
			}

			switch message := event.Message.(type) {
			case *linebot.ImageMessage:
				log.Printf("image id = %v", message.ID)

				content, err := bot.GetMessageContent(message.ID).Do()
				if err != nil {
					log.Println(err)
					continue
				}
				log.Printf("Got %d bytes (%s)", content.ContentLength, content.ContentType)

				if err := handleImages(bot, event, content); err != nil {
					log.Println(err)
					continue
				}

			case *linebot.TextMessage:
				trimmedText := strings.TrimSpace(message.Text)

				if strings.HasPrefix(trimmedText, "http") == true {
					if err := handleLinks(bot, event, trimmedText); err != nil {
						log.Println(err)
					}
					continue
				}

				if strings.HasPrefix(trimmedText, "/jbot") == true {
					if err := handleJbotCommands(bot, event, trimmedText); err != nil {
						log.Println(err)
					}
					continue
				}

				if strings.HasPrefix(trimmedText, "제이봇!") == true {
					if err := handleJbotCommands(bot, event, trimmedText); err != nil {
						log.Println(err)
					}
					continue
				}

				// 되/돼 converter
				/*
					for _, f := range strings.Fields(trimmedText) {
						log.Println(f)
						if strings.HasPrefix(f, "되") == true && strings.HasPrefix(f, "되어") == false {
							reply(bot, event.ReplyToken, strings.Replace(f, "되", "돼", 1)+"?")
						} else if strings.HasSuffix(f, "되") == true {
							reply(bot, event.ReplyToken, f[0:strings.LastIndex(f, "되")]+"돼"+"?")
						}

					}
				*/
			}

		}
	})

	//log.Fatal(http.ListenAndServeTLS(":8081", "/home/ubuntu/fullchain1.pem", "/home/ubuntu/privkey1.pem", nil))
	log.Fatal(http.ListenAndServe(":8082", nil))
	fmt.Println("vim-go")
}
